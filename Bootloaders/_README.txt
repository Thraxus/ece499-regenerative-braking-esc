----README----

"CfAR_VESC_bootloader" is the bootloader file that is used for the open-source VESC.

Theoretically, the "generic.bin" bootloader should also work, but this has not been tested.

The other bootloader file is for the non-open-source VESCs (like the VESC 60 or VESC 75).